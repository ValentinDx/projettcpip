package view;

import java.util.ArrayList;
import java.util.List;

import exception.AdresseInvalideException;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.text.Text;
import model.Adresse;
import model.AdresseIP;
import model.Conversion;

public class P1ObtenirClasseGP extends GridPane{

	private List<TextField> tfAdress;
	private List<Text> txtSeparator;
	private Label lblAdress;
	private Text txtTitre, txtAnswer;
	private Button btnSubmit;
	
	public P1ObtenirClasseGP() {
		this.setMinWidth(500);
		this.setPadding(new Insets(20.));
		this.setHgap(15.);
		this.setVgap(30.);
		
		//this.setGridLinesVisible(true);
		
		//position x position y, taille x, taille y
		this.add(getTxtTitre(), 0, 0, 3, 1);
		this.add(getLblAdress(), 0, 1, 1, 1);
		
		//cr�ation sous grille
		int j=0;
		GridPane subGP = new GridPane();
		//subGP.setGridLinesVisible(true);
		subGP.setHgap(15.);
		subGP.setVgap(5.);
		for(TextField tf : getTfAdress()) {
			subGP.add(tf, j, 0, 4, 1);
			j+=6;
		}
		j=5;
		for(Text t : getTxtSeparator()) {
			subGP.add(t,  j, 0, 1, 1);
			j+=6;
		}
		
		this.add(subGP, 1, 1, 6, 1);
		//Button
		this.add(getBtnSubmit(), 0, 3);
		
		//answer
		this.add(getTxtAnswer(), 0, 4, 5, 2);
	}
	
	// algorithm mehtod

	public String[] getAdress(List<TextField> list) {
		String[] adress = new String[4];
		int i = 0;
		for(TextField tf : list) {
			adress[i] = tf.getText();
			i++;
		}
		return adress;
	}
	
	public boolean verify() {
		for(TextField tf : getTfAdress()) {
			if(tf.getText().isBlank())
				return false;
		}
		return true;
	}
		
	// getters
	
	public Text getTxtTitre() {
		if(txtTitre == null) {
			txtTitre = new Text("Point 1 : Obtenir la classe d'une adresse IP");
			txtTitre.getStyleClass().add("title");
		}
		return txtTitre;
	}


	public List<TextField> getTfAdress() {
		if(tfAdress==null) {
			tfAdress = new ArrayList<>();
			for(int i = 0; i<4; i++) {
				tfAdress.add(new TextField());
			}
		}
		return tfAdress;
	}
	
	public Text getTxtAnswer() {
		if(txtAnswer == null) {
			txtAnswer = new Text("");
			txtAnswer.getStyleClass().add("title");
		}
		return txtAnswer;
	}
	
	

	public List<Text> getTxtSeparator() {
		if(txtSeparator == null) {
			txtSeparator = new ArrayList<>();
			for(int i = 0; i< 3; i++) {
				txtSeparator.add(new Text("."));
			}
		}
		
		return txtSeparator;
	}

	public Label getLblAdress() {
		if(lblAdress == null) {
			lblAdress = new Label("Adress IP : ");
		}
		return lblAdress;
	}

	public Button getBtnSubmit() {
		if(btnSubmit == null) {
			btnSubmit = new Button("Submit");
			btnSubmit.setStyle("-fx-background-color: linear-gradient(#99FFCC,#99CCFF);");
		}
		
		btnSubmit.setOnAction(e ->{
			if(verify()) {
				try {
					AdresseIP aIP = new AdresseIP(Conversion.stringToDecimal(getAdress(tfAdress)));
					getTxtAnswer().setText("La classe de l'Adresse IP : \""+ aIP.toString() + "\" est de classe "+ aIP.getClasseIp().toString());
					//getTxtAnswer().setText(aIP.getClasseIp().toString());
				} catch (AdresseInvalideException | NumberFormatException e1) {
					// TODO Auto-generated catch block
					//e1.printStackTrace();
					getTxtAnswer().setText("Adresse Invalide");
				}
			}
			else {
				getTxtAnswer().setText("Remplissez tout les champs!");
			}
		});
		
		return btnSubmit;
	}
	
	
	
}
