package view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;

public class ViewsSP extends StackPane{
	
	public List<Region> regions;
	
	public ViewsSP() {
		this.getChildren().addAll(getRegions());
		this.selectVisible(Accueil.class);
	}
	
	public void selectVisible(Class<?> c) {
		for(Region r : getRegions()) {
			if(r.getClass().equals(c)) {
				r.setVisible(true);
			}
			else {
				r.setVisible(false);
			}
		}
	}
	
	public List<Region> getRegions(){
		if(regions == null) {
			regions = new ArrayList<>();
			regions.addAll(Arrays.asList(
					new Accueil(),
					new P1ObtenirClasseGP(),
					new P2ObtenirInfoReseauGP(),
					new P3IPAppartenanceGP(),
					new P4IPAttribuableGP(),
					new P5MemeReseauGP()));
		}
		return regions;
	}
}
