package model;

public enum ClasseIP {

	A("A",126,16777214, new int[] {255,0,0,0}),
	B("B", 16384, 65534, new int[] {255,255,0,0}),
	C("C", 2097152, 254, new int[] {255,255,255,0}),
	D("D", 0, 0,new int[] {255,255,255,255}),
	E("E", 0, 0,new int[] {0,0,0,0});

	private String classe;
	private int nombre_Reseaux;
	private int nombre_Hotes;
	private int[] masqueParDefaut;

	ClasseIP(String string, int nbRes, int nbHotes, int[] defaultMask) {
		this.classe = string;
		this.nombre_Reseaux = nbRes;
		this.nombre_Hotes = nbHotes;
		this.masqueParDefaut = defaultMask;

	}
	
	public String toString() {
		return "Classe : " + classe +
				"\nNombre de r�seaux : " + nombre_Reseaux +
				"\nNombre d'h�tes : " + nombre_Hotes;		

	}

	public String getClasse() {
		return classe;
	}

	public int getNombre_Reseaux() {
		return nombre_Reseaux;
	}

	public int getNombre_Hotes() {
		return nombre_Hotes;
	}

	public int[] getMasqueParDefaut() {
		return masqueParDefaut.clone();
	}

}
