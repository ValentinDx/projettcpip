package model;

import java.util.Arrays;

import exception.AdresseInvalideException;

public abstract class Adresse {
	
	private int adresse[];
	private ClasseIP classeIp;
	
	public Adresse(int[] adresse) throws AdresseInvalideException {
		if(verifier(adresse)) {
			this.adresse = adresse;
			this.classeIp = determinerClasse(adresse);
		}
		else
			throw new AdresseInvalideException();
	}
	
	/**
	 * Vérifie si l'adresse est valide.
	 * 
	 * @param adresse
	 * 			l'adresse sous forme de tableau.
	 * 
	 * @return true si l'adresse est valide, false si elle ne l'est pas.
	 */
	public static boolean verifier(int[] adresse) {
		
		for(int octet : adresse) {
			if(octet != (int)octet || octet < 0 || octet > 255)
				return false;
		}
		
		return true;
	}
	
	/**
	 * Permet de déterminer la classe d'une adresse IP
	 * 
	 * @param adresse
	 * @return ClasseIP
	 */
	public static ClasseIP determinerClasse(int[] adresse) {
		
		if(adresse[0] > 0 && adresse[0] < 128)
			return ClasseIP.A;
		else if(adresse[0] >= 128 && adresse[0] < 192)
			return ClasseIP.B;
		else if(adresse[0] >= 192 && adresse[0] < 224)
			return ClasseIP.C;
		else if(adresse[0] >= 224 && adresse[0] < 240)
			return ClasseIP.D;
		else
			return ClasseIP.E;
	}

	
	public boolean equals(Adresse a) {
		return Arrays.equals(this.adresse, a.getAdresse());
	}
	
	
	public String toString() {
		return Arrays.toString(adresse).replace(", ",".").replace("[", "").replace("]", "");
	}
	
	//GETTERS & SETTERS
	
	public int[] getAdresse() {
		return adresse.clone();
	}

	public void setAdresse(int[] adresse) {
		this.adresse = adresse;
	}
	
	public ClasseIP getClasseIp() {
		return classeIp;
	}
}
