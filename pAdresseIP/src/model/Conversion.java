package model;

import exception.AdresseInvalideException;

public class Conversion {
	
	/**
	 * Transforme l'adresse IP en decimal en adresse IP binaire 8bit.
	 * 
	 * @param decimal = l'adresse ip decimale sous forme de tableau
	 * @return un tableau de string contenant l'adresse IP en binaire.
	 */
	public static String[] decimalToBinary(int[] decimal) {
		
		int[] dec = decimal.clone();
		
		String result;
		
		String[] toReturn = new String[4];
		
		for(int i=0 ; i < dec.length ; i++) {
	
	        result= "00000000"; //permet de det�riner la taille du chiffre binaire => 8 carat�re = 1octet
	        
	        int j =result.length()-1;
	        
	        while(dec[i]!=0)
	        {
	          char a[]=result.toCharArray(); //transformation du string en tableau de char.
	          
	          a[j--]= String.valueOf(dec[i]%2).charAt(0);
	          
	          result=new String(a);
	          
	          dec[i]=dec[i]/2;  

	        }
	        
	       toReturn[i] = result; 
		}
		
		return toReturn;

	}
	
	
	/**
	 * Transforme une adresse IP binaire en d�cimal.
	 * 
	 * @param binary = l'adresse Ip en binaire dans un tableau.
	 * @return Un tableau d'entier contenant chaque octer de l'ip en d�cimal.
	 */
	public static int[] binaryToDecimal(String[] binary) {
		
		int[] tabDecimal = new int[4];
		int i=0;
		
		for(String b : binary) {
			tabDecimal[i] = Integer.parseInt(b, 2); //permet de transformer un string en int. (string, base).
			i++;
		}
		
		return tabDecimal;
	}

	/**
	 * Transforme une adresse d�ciame de type String en adresse d�cimal de type int
	 * 
	 * @param sAdresse = l'adresse d�cimal en String dans un tableau
	 * @return Un tableau d'entier content l'adresse d�cimale
	 * 
	 */
	public static int[] stringToDecimal(String[] sAdresse) throws AdresseInvalideException{
		int[] tabDecimal = new int[4];
		int i=0;
		
		for(String s : sAdresse) {
			try{
				tabDecimal[i] = Integer.parseInt(s); //permet de transformer un string en int. (string, base).
				i++;
			}
			catch(NumberFormatException nfe) {
				throw new AdresseInvalideException();
			}
			
		}
		return tabDecimal;		
	}
	
	public static int[] simpleMaskToTabMask(int mask) throws AdresseInvalideException {
		if(mask<=0 || mask >=32) {
			throw new AdresseInvalideException();
		}
		int[] toReturn = new int[4];
		
		int octet = 0;
		for(int k=0; k<4; k++) {
			for(int j = 8; j>0; j--) {
				if(octet<= mask-1) {
					toReturn[k] += (Math.pow(2, j-1));
				}
				octet++;
			}
		}
		return toReturn;
	}
}
