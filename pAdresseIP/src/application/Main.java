package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import view.ApplicationAP;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	@Override
	
	public void start(Stage primaryStage) {
		try {
			AnchorPane root = new ApplicationAP();
			Scene scene = new Scene(root,500,500);
			primaryStage.setMinHeight(600);
			primaryStage.setMinWidth(1200);
			primaryStage.setMaxHeight(600);
			primaryStage.setMaxWidth(1200);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
