package view;

import java.util.ArrayList;
import java.util.List;

import exception.AdresseInvalideException;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import model.AdresseIP;
import model.AdresseReseau;
import model.Conversion;

public class P4IPAttribuableGP extends GridPane{
	private List<TextField> tfAdress, tfMasque, tfReseau;
	private TextField tfMasqueSousReseau;
	private List<Text> txtSeparator, txtSeparator2, txtSeparator3;
	private Label lblAdress, lblMasque, lblReseau;
	private Text txtTitre, txtAnswer, txtSousReseau;
	private CheckBox cbSousReseau;
	private Button btnSubmit, btnMasque;
	
	public P4IPAttribuableGP() {
		this.setMinWidth(500);
		this.setPadding(new Insets(20.));
		this.setHgap(15.);
		this.setVgap(30.);
		
		//this.setGridLinesVisible(true);
		
		//position x position y, taille x, taille y
		this.add(getTxtTitre(), 0, 0, 3, 1);
		this.add(getLblAdress(), 0, 1, 1, 1);
		
		
		//cr�ation sous grille
		GridPane subGP = new GridPane();
		//subGP.setGridLinesVisible(true);
		subGP.setHgap(15.);
		subGP.setVgap(15.);
		//adress
		toFormTxtField(getTfAdress(), getTxtSeparator(), 0, subGP);
					
		//masque
		this.add(getLblMasque(), 0, 2, 1, 1);
		toFormTxtField(getTfMasque(), getTxtSeparator2(), 1, subGP);
				
		//masque sous reseau
		subGP.add(getTxtSousReseau(), 0, 2);
		subGP.add(getTfMasqueSousReseau(), 1, 2);
		this.add(subGP, 1, 1, 6, 4);
		
		//Reseau
		this.add(getLblReseau(), 0, 4);
		toFormTxtField(getTfReseau(), getTxtSeparator3(), 3, subGP);
		
		//check button
		this.add(getCbSousReseau(), 0, 5);
		this.add(getBtnMasque(), 2, 5);
		//Button
		this.add(getBtnSubmit(), 0, 6);
		
		//answer
		this.add(getTxtAnswer(), 0, 7, 8, 3);
		
	}
	// algorithm mehtod

	public String[] getAdress(List<TextField> list) {
		String[] adress = new String[4];
		int i = 0;
		for(TextField tf : list) {
			adress[i] = tf.getText();
			i++;
		}
		return adress;
	}
	
	public boolean verify() {
		for(TextField tf : getTfAdress()) {
			if(tf.getText().isBlank())
				return false;
		}
		for(TextField tf : getTfMasque()) {
			if(tf.getText().isBlank())
				return false;
		}
		for(TextField tf : getTfReseau()) {
			if(tf.getText().isBlank())
				return false;
		}
		return true;
	}
			
	private void toFormTxtField(List<TextField> tfList, List<Text> txtSep, int y, GridPane gp) {
		// TODO Auto-generated method stub
		int x=0;
		for(TextField tf : tfList) {
			gp.add(tf, x, y, 4, 1);
			x+=6;
		}
		x=5;
		for(Text t : txtSep) {
			gp.add(t,  x, y, 1, 1);
			x+=6;
		}
		
	}
	//Algorithm getter
	public Button getBtnMasque() {
		if(btnMasque==null) {
			btnMasque= new Button("G�n�rer Masque");
			btnMasque.setStyle("-fx-background-color: linear-gradient(#CC99FF,#99CCFF);");
			btnMasque.setVisible(false);
		}
		
		btnMasque.setOnAction(e->{
			try{
				int srMasque = Integer.parseInt(tfMasqueSousReseau.getText());
				int[] masque = Conversion.simpleMaskToTabMask(srMasque);
				for(int i = 0; i<4; i++) {
					tfMasque.get(i).setText(String.valueOf(masque[i]));
				}
			}
			catch(NumberFormatException | AdresseInvalideException nfe) {
				txtAnswer.setText("Masque invalide");
			}
			
		});
		
		return btnMasque;
	}
	
	public CheckBox getCbSousReseau() {
		if(cbSousReseau==null) {
			cbSousReseau = new CheckBox("Sous r�seau ?");
		}
			
		cbSousReseau.setOnAction(e->{
			if(cbSousReseau.isSelected()) {
				for(TextField tf : getTfMasque()) {
					tf.setEditable(false);
				}
				tfMasqueSousReseau.setVisible(true);
				txtSousReseau.setVisible(true);
				btnMasque.setVisible(true);
			}
			else {
				for(TextField tf : getTfMasque()) {
					tf.setEditable(true);
				}
				tfMasqueSousReseau.setVisible(false);
				txtSousReseau.setVisible(false);
				btnMasque.setVisible(false);
			}
		});
		return cbSousReseau;
	}

	public Button getBtnSubmit() {
		if(btnSubmit == null) {
			btnSubmit = new Button("Submit");
			btnSubmit.setStyle("-fx-background-color: linear-gradient(#99FFCC,#99CCFF);");
		}
		
		btnSubmit.setOnAction(e ->{
			if(verify()) {
				try {
					AdresseIP aIP = new AdresseIP(Conversion.stringToDecimal(getAdress(tfAdress)), 
							Conversion.stringToDecimal(getAdress(tfMasque)));
					
					AdresseReseau aR= new AdresseReseau(Conversion.stringToDecimal(getAdress(tfReseau)));
					getTxtAnswer().setText("L'adresse IP  \""+ aIP.toString() + "\" "
							+ (aIP.isAttributable(aR)? "est attribuable" : "n'est pas attribuable")
							+ " au r�seau \"" +aR.toString() + "\"");
				} catch (AdresseInvalideException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					getTxtAnswer().setText("Adresse Invalide");
				}
			}
			else {
				getTxtAnswer().setText("Remplissez tout les champs!");
			}
		});
		
		return btnSubmit;
	}
		
	// getters
	
	
	public Text getTxtTitre() {
		if(txtTitre == null) {
			txtTitre = new Text("Point 4 : D�terminer si une adresse IP peut �tre attribu� � un r�seau");
			txtTitre.getStyleClass().add("title");
		}
		return txtTitre;
	}

	public List<TextField> getTfAdress() {
		if(tfAdress==null) {
			tfAdress = new ArrayList<>();
			for(int i = 0; i<4; i++) {
				tfAdress.add(new TextField());
			}
		}
		return tfAdress;
	}
	
	public Text getTxtAnswer() {
		if(txtAnswer == null) {
			txtAnswer = new Text("");
		}
		return txtAnswer;
	}
	public List<Text> getTxtSeparator() {
		if(txtSeparator == null) {
			txtSeparator = new ArrayList<>();
			for(int i = 0; i< 3; i++) {
				txtSeparator.add(new Text("."));
			}
		}
		
		return txtSeparator;
	}
	public List<Text> getTxtSeparator2() {
		if(txtSeparator2 == null) {
			txtSeparator2 = new ArrayList<>();
			for(int i = 0; i< 3; i++) {
				txtSeparator2.add(new Text("."));
			}
		}
		
		return txtSeparator2;
	}
	public List<TextField> getTfMasque() {
		if(tfMasque==null) {
		tfMasque = new ArrayList<>();
			for(int i = 0; i<4; i++) {
				tfMasque.add(new TextField());
			}
		}
		return tfMasque;
	}
	public Label getLblMasque() {
		if(lblMasque==null) {
			lblMasque = new Label("Masque : ");
		}
		return lblMasque;
	}
	public Label getLblAdress() {
		if(lblAdress == null) {
			lblAdress = new Label("Adress IP : ");
		}
		return lblAdress;
	}

	public Text getTxtSousReseau() {
		if(txtSousReseau==null) {
			txtSousReseau = new Text("\\");
			txtSousReseau.setVisible(false);
		}
		return txtSousReseau;
	}
	public TextField getTfMasqueSousReseau() {
		if(tfMasqueSousReseau==null) {
			tfMasqueSousReseau = new TextField();
			tfMasqueSousReseau.setVisible(false);
		}
		return tfMasqueSousReseau;
	}

	public List<TextField> getTfReseau() {
		if(tfReseau == null) {
			tfReseau = new ArrayList<>();
			for(int i = 0; i<4; i++) {
				tfReseau.add(new TextField());
			}
		}
		return tfReseau;
	}
	public List<Text> getTxtSeparator3() {
		if(txtSeparator3 == null) {
			txtSeparator3 = new ArrayList<>();
			for(int i = 0; i< 3; i++) {
				txtSeparator3.add(new Text("."));
			}
		}
		return txtSeparator3;
	}

	public Label getLblReseau() {
		if(lblReseau==null) {
			lblReseau = new Label("Reseau : ");
		}
		return lblReseau;
	}
}
