package model;

import exception.AdresseInvalideException;

public class AdresseReseau extends Adresse {
	
	/**
	 * Instancie une adresse r�seau.
	 * 
	 * @param adresseReseau = adresse reseau sous forme d'un tableau d'entier
	 * @throws AdresseInvalideException
	 */
	public AdresseReseau(int[] adresseReseau) throws AdresseInvalideException {
		super(adresseReseau);
	}
	
	/**
	 * Permet d'instancier une AdresseReseau � partir d'une adresse ip et d'un masque.
	 * 
	 * @param adresseIP = adresse ip au format d�cimal.
	 * @param masque = masque au format d�cimal.
	 * @throws AdresseInvalideException
	 */
	protected AdresseReseau(int[] adresseIP, int[] masque) throws AdresseInvalideException {
		super(adresseIP);
		this.setAdresse(adresseIP, masque);
	}
	
	
	/**
	 * Permet de set une adresse r�seau en fonction d'une adresse IP et d'un masque donn�.
	 * 
	 * @param decAdresse = adresse IP en d�cimal.
	 * @param decMasque = masque en d�cimal.
	 */
	private void setAdresse(int[] decAdresse,int[] decMasque) {
		
		String[] binIp = Conversion.decimalToBinary(decAdresse);
		String[] binMasque = Conversion.decimalToBinary(decMasque);
		String[] binResultat = new String[4];
		
		String octet = "";
		
		for(int i=0 ; i<binMasque.length;i++) {
			
			for(int j = 0; j< binMasque[i].length();j++) {
				
				if(binMasque[i].charAt(j) == '1')
					octet += binIp[i].charAt(j);
				else
					octet += '0';
			}
			binResultat[i] = octet;
			octet = "";
			
		}
		super.setAdresse(Conversion.binaryToDecimal(binResultat));
	}
	
	
	/**
	 * Verifie si une adresse IP est dans le r�seaux.
	 * 
	 * @param aip = instance de la classe AdresseIP
	 * @return true si l'adresse est dans le reseau, false si elle n'y est pas.
	 */
	public boolean isInNetwork(AdresseIP aip){
		
		if(aip.getAr().equals(this))
			return true;
		return false;
		
		
	}
	

}
