package model;

import java.util.Arrays;

import exception.AdresseInvalideException;

public class AdresseIP extends Adresse {
	
	private int[] masque;
	private AdresseBroadcast ab;
	private AdresseReseau ar;

	public AdresseIP(int[] adresse,int[] masque) throws AdresseInvalideException {
		
		super(adresse);
		this.masque = masque;
				
		this.ar = new AdresseReseau(adresse,masque);
		this.ab = new AdresseBroadcast(adresse,masque);
		
	}
	
	/**
	 * Permet d'instancier une adresse sans masque.
	 * @param adresse = adresse Ip sous forme d'un tableau d'int
	 * @throws AdresseInvalideException
	 */
	public AdresseIP(int[] adresse) throws AdresseInvalideException {
		super(adresse);
		this.masque = super.getClasseIp().getMasqueParDefaut();
		this.ar = new AdresseReseau(adresse,this.masque);
		this.ab = new AdresseBroadcast(adresse,this.masque);
	}
	
	/**
	 * Permet de savoir si on peut attribuer l'ip � un r�seau donn�.
	 * 
	 * @param ar = intance de la classe AdresseReseau
	 * @return true si elle est attribuable, false si elle ne l'est pas.
	 */
	public boolean isAttributable(AdresseReseau ar) {	
		
		return ar.isInNetwork(this)
				&& !this.getAb().equals(this)
				&& !ar.equals(this);
		
	}
	
	/**
	 * Verfie une adresse Ip apartient au m�me r�seau.
	 * 
	 * @param aip = instance d'AdresseIP
	 * @return true si c'est le m�me r�seau, false si ca l'est pas.
	 */
	public boolean isSameNetwork(AdresseIP aip) {
		
		return aip.getAr().equals(this.getAr());
	}
	
	
	//GETTERS & SETTERS
	
	public AdresseBroadcast getAb() {
		return this.ab;
	}
	
	public AdresseReseau getAr() {
		return this.ar;
	}
	



}
