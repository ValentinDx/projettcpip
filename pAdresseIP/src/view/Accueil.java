package view;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.geometry.Insets;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class Accueil extends Region{

	private Text text;
	
	public Accueil() {
		String filePath = "lib/adresse.jpg";
		Image image = null;
		try {
		    image = new Image(new FileInputStream(filePath));
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		}

		BackgroundImage backgroundImage =
		    new BackgroundImage(
		        image,
		        BackgroundRepeat.NO_REPEAT,  // repeat X
		        BackgroundRepeat.NO_REPEAT,  // repeat Y
		        BackgroundPosition.CENTER,   // position
		        new BackgroundSize(
		            90,   // width  = 100%
		            90,   // height = 100%
		            true,  // width is percentage
		            true,  // height is percentage
		            true,  // contain image within bounds
		            false   // cover all of Region content area
		        )
		    );

		this.setBackground(new Background(backgroundImage));
	}
	
	
}
