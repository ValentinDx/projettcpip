package view;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class ApplicationAP extends AnchorPane{

	private ViewsSP spViews;
	
	private Button btnAccueil;
	private Button btnIPApp;
	private Button btnIPAtt;
	private Button btnMemeReseau;
	private Button btnObtClass;
	private Button btnObtInfoReseau;
	
	public ApplicationAP() {
		VBox vbLeft = new VBox();
		vbLeft.getChildren().addAll(getBtnAccueil(), getBtnObtClass(), getBtnObtInfoReseau(),
				getBtnIPApp(), getBtnIPAtt(),  
				getBtnMemeReseau());
		vbLeft.setPadding(new Insets(5.));
		vbLeft.setSpacing(10.);
		
		this.getChildren().add(vbLeft);
		this.setLeftAnchor(vbLeft, 5.);
		this.setTopAnchor(vbLeft, 5.);
		
		this.getChildren().addAll(getSpViews());
		this.setTopAnchor(getSpViews(), 5.);
		this.setRightAnchor(getSpViews(), 5.);
		this.setLeftAnchor(getSpViews(), 150.);
		
	}
	
	public ViewsSP getSpViews() {
		if(spViews == null) {
			spViews = new ViewsSP();
		}
		
		return spViews;
	}

	public Button getBtnAccueil() {
		if(btnAccueil == null) {
			btnAccueil = new Button("Accueil");
		}
		btnAccueil.setOnAction(e->{
			getSpViews().selectVisible(Accueil.class);
		});
		return btnAccueil;
	}

	public Button getBtnIPApp() {
		if(btnIPApp == null) {
			btnIPApp = new Button("IP appartenance");
		}
		btnIPApp.setOnAction(e-> {
			getSpViews().selectVisible(P3IPAppartenanceGP.class);
		});
		return btnIPApp;
	}

	public Button getBtnIPAtt() {
		if(btnIPAtt == null) {
			btnIPAtt =new Button("IP attribuable");
		}
		btnIPAtt.setOnAction(e->{
			getSpViews().selectVisible(P4IPAttribuableGP.class);
		});
		return btnIPAtt;
	}

	public Button getBtnMemeReseau() {
		if(btnMemeReseau == null) {
			btnMemeReseau = new Button("Meme Reseau");
		}
		btnMemeReseau.setOnAction(e->{
			getSpViews().selectVisible(P5MemeReseauGP.class);
		});
		return btnMemeReseau;
	}

	public Button getBtnObtClass() {
		if(btnObtClass==null) {
			btnObtClass = new Button("Obtenir Classe");
		}
		btnObtClass.setOnAction(e->{
			getSpViews().selectVisible(P1ObtenirClasseGP.class);
		});
		return btnObtClass;
	}

	public Button getBtnObtInfoReseau() {
		if(btnObtInfoReseau == null) {
			btnObtInfoReseau = new Button("Obtenir info r�seau");
		}
		btnObtInfoReseau.setOnAction(e->{
			getSpViews().selectVisible(P2ObtenirInfoReseauGP.class);
		});
		
		return btnObtInfoReseau;
	}
	
	
	
	
}
