package view;

import java.util.ArrayList;
import java.util.List;

import exception.AdresseInvalideException;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import model.AdresseIP;
import model.Conversion;

public class P5MemeReseauGP extends GridPane {
	
	private List<TextField> tfAdress1, tfMasque1, tfAdress2, tfMasque2;
	private TextField tfMasqueSousReseau1, tfMasqueSousReseau2;
	private List<Text> txtSeparator1, txtSeparator2, txtSeparator3, txtSeparator4;
	private Label lblAdress1, lblMasque1, lblAdress2, lblMasque2;
	private Text txtTitre, txtAnswer, txtSousReseau1, txtSousReseau2;
	private CheckBox cbSousReseau;
	private Button btnSubmit, btnMasque;
	
	public P5MemeReseauGP() {
		this.setMinWidth(500);
		this.setPadding(new Insets(20.));
		this.setHgap(15.);
		this.setVgap(27.);
		
		//this.setGridLinesVisible(true);
		
		//position x position y, taille x, taille y
		this.add(getTxtTitre(), 0, 0, 3, 1);
		this.add(getLblAdress1(), 0, 1, 1, 1);
		
		
		//cr�ation sous grille
		GridPane subGP = new GridPane();
		//subGP.setGridLinesVisible(true);
		subGP.setHgap(15.);
		subGP.setVgap(15.);
		//adress
		toFormTxtField(getTfAdress1(), getTxtSeparator1(), 0, subGP);
		
		//masque
		this.add(getLblMasque1(), 0, 2, 1, 1);
		toFormTxtField(getTfMasque1(), getTxtSeparator2(), 1, subGP);
		
		//masque sous reseau
		subGP.add(getTxtSousReseau1(), 0, 2);
		subGP.add(getTfMasqueSousReseau1(), 1, 2);
		
		
		//adress2
		this.add(getLblAdress2(), 0,  4, 1, 1);
		toFormTxtField(getTfAdress2(), getTxtSeparator3(), 3, subGP);
		
		//masque 2
		this.add(getLblMasque2(), 0, 5, 1, 1);
		toFormTxtField(getTfMasque2(), getTxtSeparator4(), 4, subGP);
		
		//sous reseau 2
		subGP.add(getTxtSousReseau2(), 0, 5);
		subGP.add(getTfMasqueSousReseau2(), 1, 5);
		
		
		this.add(subGP, 1, 1, 5, 6);
		//check button
		this.add(getCbSousReseau(), 0, 7);
		this.add(getBtnMasque(), 2, 7);
		//Button
		this.add(getBtnSubmit(), 0, 8);
		
		//answer
		this.add(getTxtAnswer(), 0, 9, 8, 3);
	}
	

	// algorithm mehtod

		public String[] getAdress(List<TextField> list) {
			String[] adress = new String[4];
			int i = 0;
			for(TextField tf : list) {
				adress[i] = tf.getText();
				i++;
			}
			return adress;
		}
		
		public boolean verify() {
			for(int i=0; i<4; i++) {
				if(tfAdress1.get(i).getText().isBlank() || tfMasque1.get(i).getText().isBlank()||
						tfAdress2.get(i).getText().isBlank() || tfMasque2.get(i).getText().isBlank()) {
					return false;
				}
			}
			return true;
		}
		
		public void makeSeparator(List<Text> list) {
			for(int i = 0; i< 3; i++) {
				list.add(new Text("."));
			}
		}
		
		public void makeTextField(List<TextField> list) {
			for(int i = 0; i<4; i++) {
				list.add(new TextField());
			}
		}
			

		private void toFormTxtField(List<TextField> tfList, List<Text> txtSep, int y, GridPane gp) {
			// TODO Auto-generated method stub
			int x=0;
			for(TextField tf : tfList) {
				gp.add(tf, x, y, 4, 1);
				x+=6;
			}
			x=5;
			for(Text t : txtSep) {
				gp.add(t,  x, y, 1, 1);
				x+=6;
			}
			
		}
		//Algorithm getter
			public Button getBtnMasque() {
				if(btnMasque==null) {
					btnMasque= new Button("G�n�rer Masques");
					btnMasque.setStyle("-fx-background-color: linear-gradient(#CC99FF,#99CCFF);");
					btnMasque.setVisible(false);
				}
				
				btnMasque.setOnAction(e->{
					try{
						int srMasque = Integer.parseInt(tfMasqueSousReseau1.getText());
						int[] masque = Conversion.simpleMaskToTabMask(srMasque);
						
						int srMasque2 = Integer.parseInt(tfMasqueSousReseau2.getText());
						int[] masque2 = Conversion.simpleMaskToTabMask(srMasque2);
						for(int i = 0; i<4; i++) {
							tfMasque1.get(i).setText(String.valueOf(masque[i]));
							tfMasque2.get(i).setText(String.valueOf(masque2[i]));
						}
					}
					catch(NumberFormatException | AdresseInvalideException nfe) {
						txtAnswer.setText("Masque invalide");
					}
					
				});
				
				return btnMasque;
			}
			

			public CheckBox getCbSousReseau() {
				if(cbSousReseau==null) {
					cbSousReseau = new CheckBox("Sous r�seau ?");
				}
				
				cbSousReseau.setOnAction(e->{
					if(cbSousReseau.isSelected()) {
						for(TextField tf : getTfMasque1()) {
							tf.setEditable(false);
						}
						tfMasqueSousReseau1.setVisible(true);
						txtSousReseau1.setVisible(true);
						btnMasque.setVisible(true);
						
						tfMasqueSousReseau2.setVisible(true);
						txtSousReseau2.setVisible(true);
					}
					else {
						for(TextField tf : getTfMasque1()) {
							tf.setEditable(true);
						}
						tfMasqueSousReseau1.setVisible(false);
						txtSousReseau1.setVisible(false);
						btnMasque.setVisible(false);
						
						tfMasqueSousReseau2.setVisible(false);
						txtSousReseau2.setVisible(false);
					}
				});
				return cbSousReseau;
			}


			public Button getBtnSubmit() {
				if(btnSubmit == null) {
					btnSubmit = new Button("Submit");
					btnSubmit.setStyle("-fx-background-color: linear-gradient(#99FFCC,#99CCFF);");
				}
				
				btnSubmit.setOnAction(e ->{
					if(verify()) {
						try {
							AdresseIP aIP = new AdresseIP(Conversion.stringToDecimal(getAdress(tfAdress1)), 
									Conversion.stringToDecimal(getAdress(tfMasque1)));
						
							AdresseIP aIP2 = new AdresseIP(Conversion.stringToDecimal(getAdress(tfAdress2)), 
									Conversion.stringToDecimal(getAdress(tfMasque2)));
							
							txtAnswer.setText("L'adresse IP 1 \""+aIP.toString()+"\" et l'adresse IP 2 \"" + aIP2.toString()+"\" "
									+ (aIP.isSameNetwork(aIP2)? "font parties" : "ne font pas parties")+ " du m�me r�seau");
						} catch (AdresseInvalideException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							getTxtAnswer().setText("Adresse Invalide");
						}
					}
					else {
						getTxtAnswer().setText("Remplissez tout les champs!");
					}
				});
				
				return btnSubmit;
			}
			
		// getters
		
		
		public Text getTxtTitre() {
			if(txtTitre == null) {
				txtTitre = new Text("Point 5 : Verifier si deux adresses ip font partie du m�me r�seau");
				txtTitre.getStyleClass().add("title");
			}
			return txtTitre;
		}


		public List<TextField> getTfAdress1() {
			if(tfAdress1==null) {
				tfAdress1 = new ArrayList<>();
				makeTextField(tfAdress1);
				
			}
			return tfAdress1;
		}
		
		public Text getTxtAnswer() {
			if(txtAnswer == null) {
				txtAnswer = new Text("");
			}
			return txtAnswer;
		}

		public List<Text> getTxtSeparator1() {
			if(txtSeparator1 == null) {
				txtSeparator1 = new ArrayList<>();
				makeSeparator(txtSeparator1);
			}
			
			return txtSeparator1;
		}
		public List<Text> getTxtSeparator2() {
			if(txtSeparator2 == null) {
				txtSeparator2 = new ArrayList<>();
				makeSeparator(txtSeparator2);
			}
			
			return txtSeparator2;
		}
		public List<Text> getTxtSeparator3() {
			if(txtSeparator3 == null) {
				txtSeparator3 = new ArrayList<>();
				makeSeparator(txtSeparator3);
			}
			
			return txtSeparator3;
		}
		public List<Text> getTxtSeparator4() {
			if(txtSeparator4 == null) {
				txtSeparator4 = new ArrayList<>();
				makeSeparator(txtSeparator4);
			}
			
			return txtSeparator4;
		}

		public List<TextField> getTfMasque1() {
			if(tfMasque1==null) {
				tfMasque1 = new ArrayList<>();
				makeTextField(tfMasque1);
			}
			return tfMasque1;
		}
		public List<TextField> getTfAdress2() {
			if(tfAdress2==null) {
				tfAdress2 = new ArrayList<>();
				makeTextField(tfAdress2);
			}
			return tfAdress2;
		}
		public List<TextField> getTfMasque2() {
			if(tfMasque2==null) {
				tfMasque2 = new ArrayList<>();
				makeTextField(tfMasque2);
			}
			return tfMasque2;
		}

		public Label getLblMasque1() {
			if(lblMasque1==null) {
				lblMasque1 = new Label("Masque 1: ");
			}
			return lblMasque1;
		}

		public Label getLblAdress1() {
			if(lblAdress1 == null) {
				lblAdress1 = new Label("Adress IP 1: ");
			}
			return lblAdress1;
		}
		public Label getLblMasque2() {
			if(lblMasque2==null) {
				lblMasque2 = new Label("Masque 2: ");
			}
			return lblMasque2;
		}

		public Label getLblAdress2() {
			if(lblAdress2 == null) {
				lblAdress2 = new Label("Adress IP 2: ");
			}
			return lblAdress2;
		}


		public Text getTxtSousReseau1() {
			if(txtSousReseau1==null) {
				txtSousReseau1 = new Text("\\");
				txtSousReseau1.setVisible(false);
			}
			return txtSousReseau1;
		}

		public TextField getTfMasqueSousReseau1() {
			if(tfMasqueSousReseau1==null) {
				tfMasqueSousReseau1 = new TextField();
				tfMasqueSousReseau1.setVisible(false);
			}
			return tfMasqueSousReseau1;
		}
		
		public Text getTxtSousReseau2() {
			if(txtSousReseau2==null) {
				txtSousReseau2 = new Text("\\");
				txtSousReseau2.setVisible(false);
			}
			return txtSousReseau2;
		}

		public TextField getTfMasqueSousReseau2() {
			if(tfMasqueSousReseau2==null) {
				tfMasqueSousReseau2 = new TextField();
				tfMasqueSousReseau2.setVisible(false);
			}
			return tfMasqueSousReseau2;
		}

	

}
