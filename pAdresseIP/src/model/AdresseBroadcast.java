package model;

import exception.AdresseInvalideException;

public class AdresseBroadcast extends Adresse{
	
	/**
	 * Instancie une adresse broadcast.
	 * 
	 * @param adresseBroadcast = adresse broacast sous forme d'un tableau d'entier.
	 * @throws AdresseInvalideException
	 */
	public AdresseBroadcast(int[] adresseBroadcast) throws AdresseInvalideException {
		super(adresseBroadcast);
	}

	/**
	 * Permet d'instancier une adresse broadcast � partir d'une adresse IP et d'un masque.
	 * 
	 * @param adresseIP = adresse ip au format decimal.
	 * @param masque = adresse broadcast au format decimal.
	 * @throws AdresseInvalideException
	 */
	protected AdresseBroadcast(int[] adresseIP,int[] masque) throws AdresseInvalideException {
		super(adresseIP);
		this.setAdresse(adresseIP, masque);
	}
	
	
	/**
	 * Permet de set l'adresse broadcast en fonction d'une adresse IP et d'un masque donn�.
	 * 
	 * @param decAdresse = adresse IP en d�cimal. 
	 * @param decMasque = masque en d�cimal.
	 */
	private void setAdresse(int[] decAdresse,int[] decMasque) {
		
		String[] binIp = Conversion.decimalToBinary(decAdresse);
		String[] binMasque = Conversion.decimalToBinary(decMasque);
		String[] binaryResult = new String[4];
		
		String currentByte = "";
		
		for(int i=0 ; i<binMasque.length;i++) {
			
			for(int j = 0; j< binMasque[i].length();j++) {
				
				if(binMasque[i].charAt(j) == '1')
					currentByte += binIp[i].charAt(j);
				else
					currentByte += '1';
			}
			binaryResult[i] = currentByte;
			currentByte = "";
			
		}
		super.setAdresse(Conversion.binaryToDecimal(binaryResult));
	}

}
